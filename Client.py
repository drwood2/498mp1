import cv2
import os
import requests
import json

# define variables
addr = '<your_server_address>'
test_url = addr + '<your_endpoint>'
content_type = 'image/jpeg'
headers = {'content-type': content_type}
video = cv2.VideoCapture(0)
count = 0

# looping
while True:
    count = count + 1
    check, frame = video.read()
    # print log in terminal
    print(check)
    print(frame)
    # setup color in frame
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("captureing", gray)
    # define image name
    image_name = 'image_{}.jpg'.format(count)
    # write image
    cv2.imwrite(image_name, frame)
    # read image
    img = cv2.imread(image_name)
    # encoded image after read
    _, img_encoded = cv2.imencode('.jpg', img)
    # post imsge to server
    response = requests.post(test_url, data=img_encoded.tostring(), headers=headers)
    # remove image after write
    os.remove(image_name)
    # setup key for close capturing
    key = cv2.waitKey(2)
    if key == ord('q'):
        break

print('number of photos: ', count)
video.release()
cv2.destroyAllWindows