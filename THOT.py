import io
import picamera
import cv2
import numpy
import sys
import socket

from io import BytesIO
from time import sleep
from picamera import PiCamera

def face_detect():
    stream = BytesIO()
    #camera = PiCamera()
    with PiCamera() as camera:
        #camera.start_preview()
        #sleep(2)
        camera.resolution = (320, 240)
        camera.capture(stream, format='jpeg')

    #Create a memory stream so photos doesn't need to be saved in a file


    #Get the picture (low resolution, so it should be quite fast)
    #Here you can also specify other parameters (e.g.:rotate the image)

    

    #Convert the picture into a numpy array
    buff = numpy.frombuffer(stream.getvalue(), dtype=numpy.uint8)

    #Now creates an OpenCV image
    image = cv2.imdecode(buff, 1)

    #https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml
    #Load a cascade file for detecting faces
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")

    #Convert to grayscale
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)

    #Look for faces in the image using the loaded cascade file
    faces = face_cascade.detectMultiScale(gray, 1.1, 5)

    print ("Found {}" + str(len(faces)) + " face(s)")

    #Draw a rectangle around every found face
    for (x,y,w,h) in faces:
        cv2.rectangle(image,(x,y),(x+w,y+h),(255,255,0),4)

    #Save the result image
    cv2.imwrite('result.jpg',image)
    return len(faces)
    
if __name__ == "__main__":
    UDP_IP="192.168.0.126"
    UDP_PORT=24
    MESSAGE=b'1Test'
    print(socket.gethostname())
    print ("UDP target IP:", UDP_IP)
    print ("UDP target port:", UDP_PORT)
    print ("message:", MESSAGE)
    sock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM ) # UDP
    server_address = (UDP_IP, UDP_PORT)
    #try:
    #sock.bind(server_address)
    #error = sock.sendto(MESSAGE, server_address)
    
    while(1):
        retval = 0
        retval = face_detect()
        if(retval>0):
            error=sock.sendto(MESSAGE,server_address)
        