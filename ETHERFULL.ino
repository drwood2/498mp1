// CAN Receive Example
//

#include <mcp_can.h>
#include <SPI.h>
#include <Ethernet.h>

long unsigned int rxId;
unsigned char len = 0;
unsigned char rxBuf[8];
char msgString[128];                        // Array to store serial string

#define CAN0_INT 2                              // Set INT to pin 2
MCP_CAN CAN0(10);                               // Set CS to pin 10

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {

  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
};

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  // buffer to hold incoming packet,
char ReplyBuffer[] = "acknowledged";
EthernetUDP Udp;
unsigned int localPort = 24;

void setup()
{
  Serial.begin(115200);

  // Initialize MCP2515 running at 16MHz with a baudrate of 500kb/s and the masks and filters disabled.
  if (CAN0.begin(MCP_ANY, CAN_500KBPS, MCP_16MHZ) == CAN_OK)
    Serial.println("MCP2515 Initialized Successfully!");
  else
    Serial.println("Error Initializing MCP2515...");

  CAN0.setMode(MCP_NORMAL);                     // Set operation mode to normal so the MCP2515 sends acks to received data.

  pinMode(CAN0_INT, INPUT);                            // Configuring pin for /INT input

  Serial.println("MCP2515 Library Receive Example...");

  // start the Ethernet connection:

  Serial.println("Initialize Ethernet with DHCP:");

  if (Ethernet.begin(mac) == 0) {

    Serial.println("Failed to configure Ethernet using DHCP");

    if (Ethernet.hardwareStatus() == EthernetNoHardware) {

      Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");

    } else if (Ethernet.linkStatus() == LinkOFF) {

      Serial.println("Ethernet cable is not connected.");

    }

    // no point in carrying on, so do nothing forevermore:

    while (true) {

      delay(1);

    }

  }

  // print your local IP address:

  Serial.print("My IP address: ");

  Serial.println(Ethernet.localIP());

  Udp.begin(24);
  Serial.println("Listening for msgs\n on");
  Serial.println(Ethernet.localIP());
  Serial.println(Udp.localPort());
}

byte data[8] = {0x11, 0x11, 0x32, 0x23, 0x54, 0x55, 0x11, 0x00};
int dist = 0;
int led_set = 0;
int eth_set = 0;
int delay_counter = 0;
int sample_list[3]={0,0,0};





void loop()
{
  if (!digitalRead(CAN0_INT))                        // If CAN0_INT pin is low, read receive buffer
  {
    
    CAN0.readMsgBuf(&rxId, &len, rxBuf);      // Read data: len = data length, buf = data byte(s)
    if (rxBuf[0] < 20) {
      //Serial.print("Less than 20: <");
      //Serial.print(rxBuf[0]);
      led_set = 1;
    }
    else {
      //Serial.print("Not Less than 20: >");
      //Serial.print(rxBuf[0]);
      led_set = 0;
    }
    if ((rxId & 0x80000000) == 0x80000000)    // Determine if ID is standard (11 bits) or extended (29 bits)
      sprintf(msgString, "Extended ID: 0x%.8lX  DLC: %1d  Data:", (rxId & 0x1FFFFFFF), len);
    else
      sprintf(msgString, "Standard ID: 0x%.3lX       DLC: %1d  Data:", rxId, len);
    //Serial.print("|");
    //Serial.print(rxBuf[0]);
    //Serial.print("| msgString[1] \n");
    //Serial.print("msgString is \n");
    //Serial.print(msgString);
    //Serial.print("data[7]:\n |");
    //Serial.print(data[7]);
    //Serial.print("|");
    //Serial.print("data[0]:\n |");
    //Serial.print(data[0]);
    //Serial.print("|");

    if ((rxId & 0x40000000) == 0x40000000) {  // Determine if message is a remote request frame.
      sprintf(msgString, " REMOTE REQUEST FRAME");
      //Serial.print(msgString);
      if (msgString[0] < 20) {
        //Serial.print("Less than 20: ");
        //Serial.print(msgString[0]);
        led_set = 1;
      }
      else {
        led_set = 0;
      }
    } else {

      for (byte i = 0; i < len; i++) {


//        data[7] = led_set;
//        byte sndStat = CAN0.sendMsgBuf(0x099, 0, 8, data);
//        if (sndStat == CAN_OK) {
//          //Serial.println("Message Sent Successfully!\n");
//          //Serial.print("Data[7] is ");
//          //Serial.print(data[7]);
//        } else {
//          //Serial.println("Error Sending Message...");
//        }
      }
    }

    //Serial.println();
  }
  else{
    led_set = 0;
  }
  

  switch (Ethernet.maintain()) {

    case 1:

      //renewed fail

      Serial.println("Error: renewed fail");

      break;

    case 2:

      //renewed success

      Serial.println("Renewed success");

      //print your local IP address:

      Serial.print("My IP address: ");

      Serial.println(Ethernet.localIP());

      break;

    case 3:

      //rebind fail

      Serial.println("Error: rebind fail");

      break;

    case 4:

      //rebind success

      Serial.println("Rebind success");

      //print your local IP address:

      Serial.print("My IP address: ");

      Serial.println(Ethernet.localIP());

      break;

    default:

      //nothing happened

      break;

  }
  eth_set=0;
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer[0]);
    eth_set = packetBuffer[0];
    if(eth_set){
      Serial.println("LED According To Ethernet");
    }

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer);
    Udp.endPacket();
  }

  sample_list[2] = sample_list[1];
  sample_list[1] = sample_list[0];
  sample_list[0] = (led_set)||(eth_set);
  data[7] = 0;
  int i=0;
  for(i = 0; i<3;i++){ 
    if(sample_list[i]){
      data[7] = 1;
    }
  }
  
  byte sndStat = CAN0.sendMsgBuf(0x099, 0, 8, data);
  
  delay(100);
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/
