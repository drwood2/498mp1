#include <mcp_can.h>
#include <SPI.h>

long unsigned int rxId;
unsigned char len = 0;
unsigned char rxBuf[8];
char msgString[128];   

const int pingPin = 7; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 6; // Echo Pin of Ultrasonic Sensor
const int vccOut = 8;
int led_set=0;

const int LEDVCC = 5;
const int LEDRET = 4; 

#define CAN0_INT 2                              // Set INT to pin 2
MCP_CAN CAN0(10);     // Set CS to pin 10
void setup() {
  Serial.begin(115200); // Starting Serial Terminal
  if(CAN0.begin(MCP_ANY, CAN_500KBPS, MCP_16MHZ) == CAN_OK) Serial.println("MCP2515 Initialized Successfully!");
  else Serial.println("Error Initializing MCP2515...");

  pinMode(LEDVCC,OUTPUT);
  pinMode(LEDRET,OUTPUT);
  pinMode(CAN0_INT, INPUT);                            // Configuring pin for /INT input

  digitalWrite(LEDVCC,HIGH);
  digitalWrite(LEDRET,LOW);

  CAN0.setMode(MCP_NORMAL);   // Change to normal mode to allow messages to be transmitted
}

byte data[8] = {0x11, 0x11, 0x32, 0x23, 0x54, 0x55, 0x11, 0x00};
int old_data=0;
int counter=0;

void loop() {
   long duration, inches, cm;

   
   
   pinMode(pingPin, OUTPUT);
   digitalWrite(pingPin, LOW);
   delayMicroseconds(2);
   digitalWrite(pingPin, HIGH);
   delayMicroseconds(10);
   digitalWrite(pingPin, LOW);
   pinMode(echoPin, INPUT);
   pinMode(vccOut, OUTPUT);
   digitalWrite(vccOut,HIGH);
   duration = pulseIn(echoPin, HIGH);
   inches = microsecondsToInches(duration);
   cm = microsecondsToCentimeters(duration);
   Serial.print(inches);
   Serial.print("in, ");
   Serial.print(cm);
   Serial.print("cm");
   Serial.println();
   // send data:  ID = 0x100, Standard CAN Frame, Data length = 8 bytes, 'data' = array of data bytes to send
  if(old_data==cm){
    Serial.print("counter++:");
    Serial.print(counter);
    Serial.print("\n\n");
    counter+=1;
  }
  else{
    counter=0;
    Serial.print("counter=0");
  }

   if(counter>0){
    if(cm>0){
      
    
      data[0] = cm;
      Serial.print(data[0]);
      Serial.print("\n");
      Serial.print(data[1]);
      byte sndStat = CAN0.sendMsgBuf(0x100, 0, 8, data);
      if(sndStat == CAN_OK){
         Serial.println("Message Sent Successfully!");
      } else {
        Serial.println("Error Sending Message...");
      }
    }
   }
   old_data=cm;
   if(!digitalRead(CAN0_INT))                         // If CAN0_INT pin is low, read receive buffer
    {
      CAN0.readMsgBuf(&rxId, &len, rxBuf);      // Read data: len = data length, buf = data byte(s)
      led_set = rxBuf[7];
      Serial.print("LED is: |");
      Serial.print(led_set);
      Serial.print("|\n");
      digitalWrite(LEDVCC,led_set);
     
    }
   delay(100);
}

long microsecondsToInches(long microseconds) {
   return microseconds / 74 / 2;
}

long microsecondsToCentimeters(long microseconds) {
   return microseconds / 29 / 2;
}
